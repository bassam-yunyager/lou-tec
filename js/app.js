function changeSec(section) {
    console.log(section)
    $('section').hide();
    if (section === 3) {
        section--;
        $('#sec-' + section).addClass('is-section-3')
        $('#tarif-info').slideDown()
    } else {
        $('section').removeClass('is-section-3')
        $('#tarif-info').slideUp()
    }

    if (section === 1) {
        $('#tarif-info-fournisseur').slideDown()
    } else {
        $('#tarif-info-fournisseur').slideUp()
    }

    $('#sec-' + section).slideDown()
}

$(function() {
    changeSec(3)
})

EMF_jQuery(window).load(function() {
    post_message_for_frame_height("obbIp40f0qu9lD");
});

EMF_jQuery(function() {
    toggle_emf_element(EMF_jQuery('#emf-li-1 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-1').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-7 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-7').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-11 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-11').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-14 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-14').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-15 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-15').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-16 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-16').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-17 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-17').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-18 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-18').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-19 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-19').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-20 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-20').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-21 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-21').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-22 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-22').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-23 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-23').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-24 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-24').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-25 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-25').find('input:checked, .emf-allow-other input').change();
    toggle_emf_element(EMF_jQuery('#emf-li-26 .emf-allow-other input'), false);
    EMF_jQuery('#emf-li-26').find('input:checked, .emf-allow-other input').change();;

    EMF_jQuery("#emf-form").validationEngine({
        validationEventTriggers: "blur",
        scroll: true
    });
    // DISABLE PREVENTING SUBMIT: TODO: DISCUSS and remove
    //prevent_duplicate_submission(EMF_jQuery("#emf-form"));

    $("input[emf_mask_input=true]").dPassword();

    if (EMF_jQuery('#captcha_image').length > 0) {
        on_captcha_image_load();
    }

    EMF_jQuery('.emf-field-grid td').click(function(event) {
        // 		if(!event.target.tagName || event.target.tagName.toLowerCase()!='td') return;

        EMF_jQuery(this).find('input[type=checkbox],input[type=radio]').click();
    });

    EMF_jQuery('input[type=checkbox],input[type=radio]').click(function(event) {
        event.stopPropagation();
    });


    EMF_jQuery("#emf-form ul li").mousedown(highlight_field_on_mousedown);
    EMF_jQuery("#emf-form ul li input, #emf-form ul li textarea, #emf-form ul li select").focus(highlight_field_on_focus);

    var form_obj = EMF_jQuery("#emf-container form");
    if (form_obj.length > 0 && form_obj.attr('action').indexOf('#') == -1 && window.location.hash) {
        form_obj.attr('action', form_obj.attr('action') + window.location.hash);
    }

    init_rules();

    enable_session_when_cookie_disabled();

    detect_unsupported_browser();

    randomize_field_content();



});

var emf_widgets = {
    text: function(index) {
        return $("#element_" + index).val();
    },
    number: function(index) {
        return $("#element_" + index).val();
    },
    textarea: function(index) {
        return $("#element_" + index).val();
    },
    new_checkbox: function(index) {
        var arr = new Array();
        $("input[name='element_" + index + "[]']:checked").each(function() {
            arr[arr.length] = this.value;
        });
        var result = arr.join(", ");
        return result;
    },
    radio: function(index) {
        var result = "";
        $("input[name=element_" + index + "]:checked").each(function() {
            result = this.value;
        });
        return result;
    },
    select: function(index) {
        return $("#element_" + index).val();
    },
    email: function(index) {
        return $("#element_" + index).val();
    },
    phone: function(index) {
        var arr = new Array();
        $("input[id^=element_" + index + "_]").each(function() {
            arr[arr.length] = this.value;
        });

        var result = "";
        if (arr.length > 0) {
            result = arr.join("-");
        } else {
            result = $("#element_" + index).val();
        }
        return result;
    },
    datetime: function(index) {
        var result = "";

        var date_part = "";
        if ($("#element_" + index + "_year").length == 1) {
            date_part = $("#element_" + index + "_year-mm").val() + "/" + $("#element_" + index + "_year-dd").val() + "/" + $("#element_" + index + "_year").val();
        }

        var time_part = "";
        if ($("#element_" + index + "_hour").length == 1) {
            time_part = $("#element_" + index + "_hour").val() + ":" + $("#element_" + index + "_minute").val() + " " + $("#element_" + index + "_ampm").val();
        }

        if (date_part && time_part) {
            result = date_part + " " + time_part;
        } else {
            result = date_part ? date_part : time_part;
        }

        return result;
    },
    url: function(index) {
        return $("#element_" + index).val();
    },
    file: function(index) {
        return $("#element_" + index).val();
    },
    Image: function(index) {
        return $("#element_" + index).val();
    },
    new_select_multiple: function(index) {
        return $("#element_" + index).val();
    },
    price: function(index) {
        var result = "";
        var arr = new Array();
        $("input[id^=element_" + index + "_]").each(function() {
            arr[arr.length] = this.value;
        });
        result = arr.join(".");
        return result;
    },
    hidden: function(index) {
        return $("#element_" + index).val();
    },
    unique_id: function(index) {
        return $("#element_" + index).val();
    },
    section_break: function(index) {
        return "";
    },
    page_break: function(index) {
        return "";
    },
    signature: function(index) {
        return $("#element_" + index).val();
    },
    star_rating: function(index) {
        var result = "";
        $("input[name=element_" + index + "]:checked").each(function() {
            result = this.value;
        });
        return result;
    },
    scale_rating: function(index) {
        var result = "";
        $("input[name=element_" + index + "]:checked").each(function() {
            result = this.value;
        });
        return result;
    },
    deprecated: function(index) {
        return $("#element_" + index).val();
    },
    address: function(index) {
        var result = "";
        var element_arr = $("input,select").filter("[name='element_" + index + "[]']").toArray();
        result = element_arr[0].value + " " + element_arr[1].value + "\n" +
            element_arr[2].value + "," + element_arr[3].value + " " + element_arr[4].value + "\n" +
            element_arr[5].value;
        return result;
    },
    name: function(index) {
        var arr = new Array();
        $("input[id^=element_" + index + "_]").each(function() {
            arr[arr.length] = this.value;
        });
        var result = arr.join(" ");
        return result;
    },
    checkbox: function(index) {
        var arr = new Array();
        $("input[name='element_" + index + "[]']:checked").each(function() {
            arr[arr.length] = this.value;
        });
        var result = arr.join(", ");
        return result;
    },
    select_multiple: function(index) {
        return $("#element_" + index).val();
    }
};

var emf_condition_id_to_js_map = {
    5: function(field_value, value) {
        return field_value == value;
    },
    6: function(field_value, value) {
        return field_value != value;
    },
    1: function(field_value, value) {
        return field_value.indexOf(value) > -1;
    },
    2: function(field_value, value) {
        return field_value.indexOf(value) == -1;
    },
    3: function(field_value, value) {
        return field_value.indexOf(value) == 0;
    },
    4: function(field_value, value) {
        return field_value.indexOf(value) == field_value.length - value.length;
    },
    7: function(field_value, value) {
        return parseFloat(field_value) == parseFloat(value);
    },
    8: function(field_value, value) {
        return parseFloat(field_value) > parseFloat(value);
    },
    9: function(field_value, value) {
        return parseFloat(field_value) < parseFloat(value);
    },
    10: function(field_value, value) {
        var date_for_field_value = Date.parse(field_value);
        var date_for_value = Date.parse(value);
        if (date_for_field_value && date_for_value) {
            return date_for_field_value == date_for_value;
        }
        return false;
    },
    11: function(field_value, value) {
        var date_for_field_value = Date.parse(field_value);
        var date_for_value = Date.parse(value);
        if (date_for_field_value && date_for_value) {
            return date_for_field_value < date_for_value;
        }
        return false;
    },
    12: function(field_value, value) {
        var date_for_field_value = Date.parse(field_value);
        var date_for_value = Date.parse(value);
        if (date_for_field_value && date_for_value) {
            return date_for_field_value > date_for_value;
        }
        return false;
    }
};
var emf_group_to_field_rules_map = [
    [{
        "Id": "11139341",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "0",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "6",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413110",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 0,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139342",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "1",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413292",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 19,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139343",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "1",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "FOURNISSEUR",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413292",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 19,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139344",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "2",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413238",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 20,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139345",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "2",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "FOURNISSEUR",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413238",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 20,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139346",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "3",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413239",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 21,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139347",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "3",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413239",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 21,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139348",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "4",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413293",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 22,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139349",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "4",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413293",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 22,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139350",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "5",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413128",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 25,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139351",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "5",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "FOURNISSEUR",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413128",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 25,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139352",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "6",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413129",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 26,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139353",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "6",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "FOURNISSEUR",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413129",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 26,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139354",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "7",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413126",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 17,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139355",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "7",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "FOURNISSEUR",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413126",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 17,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139356",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "8",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413127",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 18,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139357",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "8",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "FOURNISSEUR",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413127",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 18,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139358",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "9",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413233",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 23,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139359",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "9",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "FOURNISSEUR",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413233",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 23,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139360",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "10",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "2",
        "ResultContent": "68413232",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 24,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139361",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "10",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "FOURNISSEUR",
        "AndOr": "2",
        "ResultAction": "2",
        "ResultContent": "68413232",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 24,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139362",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "11",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "1",
        "ResultContent": "68413123",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 14,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139363",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "11",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "2",
        "ResultAction": "1",
        "ResultContent": "68413123",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 14,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139364",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "12",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "1",
        "ResultContent": "68413124",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 15,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139365",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "12",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "2",
        "ResultAction": "1",
        "ResultContent": "68413124",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 15,
        "ResultContentWidgetName": "radio"
    }],
    [{
        "Id": "11139366",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "13",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "MEMBRE LOU-TEC",
        "AndOr": "1",
        "ResultAction": "1",
        "ResultContent": "68413125",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 16,
        "ResultContentWidgetName": "radio"
    }, {
        "Id": "11139367",
        "FormId": "4510577",
        "Type": "2",
        "GroupNo": "13",
        "ConditionField": "68412956",
        "ConditionSubField": "0",
        "Operation": "5",
        "Value": "DIRECTION GROUPE LOU-TEC",
        "AndOr": "2",
        "ResultAction": "1",
        "ResultContent": "68413125",
        "ResultContent2": null,
        "MessageFormat": "",
        "EmailType": "",
        "EmailSenderName": "",
        "ReplyEmail": "",
        "EmailSubject": "",
        "EmailBody": "",
        "IncludeUserEntry": "0",
        "EmailFormat": "",
        "ConditionFieldIndex": 1,
        "ConditionFieldWidgetName": "radio",
        "ResultContentIndex": 16,
        "ResultContentWidgetName": "radio"
    }]
];
var emf_group_to_page_rules_for_confirmation_map = [];

var emf_cart = null;
var emf_page_info = {
    current_page_index: 0,
    page_element_index_min: 0,
    page_element_index_max: 26
};
var emf_index_to_value_map = null;
var emf_form_visit_id = "obbIp40f0qu9lD";

var emf_index_to_option_map = [];
